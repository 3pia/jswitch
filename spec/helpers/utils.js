/**
 * Test utilities.
 */

// node modules
const fs = require("fs");

// npm modules
const request = require("request-promise-native");
const YAML = require("yamljs");

const fixtureCredentials = {
  username: "Fixture",
  email: "test@fixture.com",
  password: "fixturepassword"
};

global.hookFlags = {
  "server.setup.before": false,
  "server.setup.after": false,
  "server.listen.before": false,
  "server.listen.after": false,
  "server.configure.after": false,
  "middleware.setup.before": false,
  "middleware.setup.after": false,
  "app.setup.before": false,
  "app.setup.after": false,
  "socket.start.before": false,
  "socket.start.after": false
};

global.eventFlags = JSON.parse(JSON.stringify(global.hookFlags));

const configFile = "spec/support/testconf.yml";
const config = YAML.parse(fs.readFileSync(configFile, { encoding: "utf8" }));

while (config.server.base.endsWith("/")) {
  config.server.base = config.server.base.slice(0, -1);
}

const { host, port, base } = config.server;
const { name, pass } = config.server.auth;
const hostURL = `https://${name}:${pass}@${host}`;
const baseURL = `${hostURL}:${port}${base}`;
const authlessBaseURL = `https://${host}:${port}${base}`;

/**
 * Exports.
 */

module.exports = {
  createFixtureUser: async () => {
    const options = {
      json: fixtureCredentials
    };
    await request.put(`${baseURL}/api/user`, options);
  },
  loginFixtureUser: async cookieJar => {
    const options = {
      json: fixtureCredentials,
      jar: cookieJar
    };
    await request.post(`${baseURL}/api/session`, options);
  },
  setupArgs: args => {
    args.configFile = configFile;
    args.host = host;
    args.port = port;
    args.base = base + "/";
    args.logFile = "spec/support/log.txt";
    args.logLevel = "error";
    return args;
  },
  setupEvents: events => {
    for (const eventName of Object.keys(global.eventFlags)) {
      events.on(eventName, () => {
        global.eventFlags[eventName] = true;
      });
    }
  },
  setupHooks: hooks => {
    for (const hookName of Object.keys(global.hookFlags)) {
      hooks.set(hookName, () => {
        global.hookFlags[hookName] = true;
      });
    }
  },
  baseURL,
  hostURL,
  authlessBaseURL,
  config
};
