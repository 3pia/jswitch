/* global beforeAll afterAll: true */

/**
 * This script starts the server for the test environment.
 */

// setup environment
require("../../env");

// node modules
const fs = require("fs");

// local mpdules
const Server = require("../../lib/server");
const cli = require("../../lib/cli");
const utils = require("./utils.js");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// start the server before tests
beforeAll(async () => {
  if (fs.existsSync(utils.config.db.options.storage)) {
    fs.unlinkSync(utils.config.db.options.storage);
  }

  const args = utils.setupArgs(cli());

  if (fs.existsSync(args.logFile)) {
    fs.unlinkSync(args.logFile);
  }

  this.server = new Server(args);
  utils.setupEvents(this.server.events);
  utils.setupHooks(this.server.hooks);
  await this.server.setup();
  await this.server.listen();

  await utils.createFixtureUser();
});

// cleanup afterwards
afterAll(async () => {
  await this.server.shutdown("tests finished", null, "log this one");
  if (fs.existsSync(utils.config.db.options.storage)) {
    fs.unlinkSync(utils.config.db.options.storage);
  }
});
