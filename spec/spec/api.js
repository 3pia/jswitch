/* global describe it beforeAll expect jasmine: true */

/**
 * Core API tests.
 */

// npm modules
const request = require("request-promise-native");

// local modules
const utils = require("../helpers/utils.js");

describe("core api", () => {
  it("should return pong", async () => {
    const res = await request.get(`${utils.baseURL}/api/ping`);
    expect(res).toBe("pong");
  });
});

describe("user api", () => {
  const userEndpoint = `${utils.baseURL}/api/user`;
  const sessionEndpoint = `${utils.baseURL}/api/session`;

  const cookieJar = request.jar();

  // helper to extract cookies by name
  const getCookie = name => {
    const cookies = cookieJar.getCookies(utils.hostURL);
    for (const cookie of cookies) {
      if (cookie.key == name) {
        return cookie;
      }
    }
    return null;
  };

  const options = {
    json: {
      username: "TestUser",
      email: "test@user.com",
      password: "totallysecure"
    },
    jar: cookieJar
  };

  const invalidOptions = {
    json: {
      username: "TestUser",
      email: "test@user.com"
    },
    jar: cookieJar
  };

  const badOptions = {
    json: {
      username: "TestUser",
      email: "test@user.com",
      password: "totallywrong"
    },
    jar: cookieJar
  };

  it("should allow for user registration", async () => {
    const res = await request.put(userEndpoint, options);
    expect(res).toEqual(
      jasmine.objectContaining({
        status: 200,
        msg: null
      })
    );
  });

  it("should allow users to log out", async () => {
    await request.delete(sessionEndpoint, { jar: cookieJar });
    expect(getCookie("sid").value).toEqual("");
  });

  it("should not allow users to log in using the wrong password", async () => {
    try {
      const res = await request.post(sessionEndpoint, badOptions);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(getCookie("sid").value).toEqual("");
      expect(e.message).toContain("Username and password do not match.");
    }
  });

  it("should be impossible to login without password", async () => {
    try {
      const res = await request.post(sessionEndpoint, invalidOptions);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(getCookie("sid").value).toEqual("");
      expect(e.message).toContain("Invalid request body.");
    }
  });

  it("should be impossible to login if the user does not exist", async () => {
    try {
      const res = await request.post(sessionEndpoint, badOptions);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(getCookie("sid").value).toEqual("");
      expect(e.message).toContain("Username and password do not match.");
    }
  });

  it("should reject registration of existing users", async () => {
    try {
      const res = await request.put(userEndpoint, options);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(e.message).toContain("User already exists.");
    }
  });

  it("should reject registration of a user without password", async () => {
    try {
      const res = await request.put(userEndpoint, invalidOptions);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(getCookie("sid").value).toEqual("");
      expect(e.message).toContain("Invalid request body.");
    }
  });

  it("should deny user deletion without correct cookie", async () => {
    try {
      const res = await request.delete(userEndpoint);
      expect(res).toEqual("Request should fail");
    } catch (e) {
      expect(e.message).toContain("Forbidden.");
    }
  });

  it("should allow users to log in", async () => {
    await request.post(sessionEndpoint, options);
    expect(getCookie("sid").value).toEqual(jasmine.any(String));
  });

  it("should allow for user deletion", async () => {
    const res = await request.delete(userEndpoint, options);
    expect(res).toEqual(
      jasmine.objectContaining({
        status: 200,
        msg: null
      })
    );
  });
});

describe("public static pages routes", () => {
  it("should serve /login", async () => {
    const res = await request.get(`${utils.baseURL}/login`, {
      resolveWithFullResponse: true
    });
    expect(res.statusCode).toEqual(200);
    expect(res.body).toContain("html");
  });

  it("should redirect to /login from / and /index", async () => {
    const login = await request.get(`${utils.baseURL}/login`, {
      resolveWithFullResponse: true
    });
    for (let endpoint of ["", "index"]) {
      const redirect = await request.get(`${utils.baseURL}/${endpoint}`, {
        resolveWithFullResponse: true
      });
      expect(redirect.statusCode).toEqual(200);
      expect(redirect.body).toEqual(login.body);
    }
  });
});

describe("private static page routes", () => {
  const cookieJar = request.jar();
  const options = {
    jar: cookieJar,
    resolveWithFullResponse: true
  };

  beforeAll(async () => {
    await utils.loginFixtureUser(cookieJar);
  });

  it("should serve /index at /", async () => {
    const index = await request.get(`${utils.baseURL}/index`, options);
    const slash = await request.get(`${utils.baseURL}/`, options);
    const wrong = await request.get(`${utils.baseURL}/index`, {
      resolveWithFullResponse: true
    });

    expect(index.statusCode).toEqual(200);
    expect(slash.statusCode).toEqual(200);
    expect(index.body).toEqual(slash.body);
    expect(index.body).not.toEqual(wrong.body);
  });
});
