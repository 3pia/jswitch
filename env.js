/**
 * Environment prepartion.
 */

// change into the project directory
process.chdir(__dirname);

// set JSWITCH_BASE to the project directory
process.env.JSWITCH_BASE = __dirname;

// set NODE_ENV explicitely to "development" when empty or check for valid value when set
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = "production";
} else if (!["production", "development"].includes(process.env.NODE_ENV)) {
  throw new Error(
    `NODE_ENV ${
      process.env.NODE_ENV
    } unknown, must be production or development`
  );
}
