# jswitch Docker Setup

[Docker Hub](https://hub.docker.com/r/3pia).

### Starting the Server

```bash
docker run --rm -ti -p 4282:4282 3pia/jswitch
```

For configuration via environment variables, see the [docker-startup.sh](./jswitch/docker-startup.sh) script.

### Developing jswitch through Docker

```bash
# checkout
git checkout git@git.rwth-aachen.de:3pia/jswitch.git
cd jswitch

# run the server inside docker
docker run --name jsdev -ti -p 4282:4282 -v `pwd`:/root/jswitch 3pia/jswitch
```

This container named "jsdev" is not deleted when you stop it (no `--rm` parameter). If you want to reattach it to start developing again, do:

```bash
docker start jsdev && docker attach jsdev
```
