/**
 * Script to start the serving process.
 */

// local modules
require("./env");
const Server = require("./lib/server");
const cli = require("./lib/cli");

/**
 * Start the server.
 */

const server = new Server(cli());
server.setup().then(() => server.listen());
