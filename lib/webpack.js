/**
 * Webpack configuration.
 */

// npm modules
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

// local modules
const util = require("./util");

module.exports = async function(server) {
  /**
   * Helpers and plugins.
   */

  // list of plugins to use
  const plugins = [];

  // some configs depend on the node env, so store if it's development, i.e., not production
  const isDev = process.env.NODE_ENV !== "production";

  // uglify when not in development
  if (!isDev) {
    plugins.push(new UglifyJSPlugin());
  }

  // extractor which, when not in "development", extracts styles as text into dedicated files
  const extractStyles = new ExtractTextPlugin({
    filename: "[name]-[contenthash].css",
    disable: isDev
  });
  plugins.push(extractStyles);

  // custom logging
  const logger = server.logger.sub("webpack");
  const logConfig = server.config.client.webpack.logging;
  logConfig.colors = util.isTTY() && logConfig.colors;

  const logStats = stats => {
    let level = server.config.client.webpack.logging.defaultLevel;
    if (stats.hasErrors()) {
      level = "error";
    } else if (stats.hasWarnings()) {
      level = "warning";
    }
    logger.log(level, "bundling done,", stats.toString(logConfig).trim());
  };

  /**
   * Loader options.
   */

  const cssOptions = {
    root: server.config.server.base,
    minimize: !isDev
  };

  const lessOptions = {
    cleanCss: true
  };

  const htmlOptions = {
    attrs: [":data-src"],
    minimize: !isDev,
    minifyJS: !isDev,
    minifyCSS: !isDev,
    removeComments: !isDev
  };

  /**
   * Define webpack options and run.
   */

  const options = Object.assign(
    {
      entry: Object.assign(
        {},
        {
          "static/bundle/index": "./static/js/index.js",
          "static/bundle/login": "./static/js/login.js"
        }
      ),
      output: {
        path: process.env.JSWITCH_BASE,
        filename: "[name].js"
      },
      module: {
        rules: [
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader",
              options: {
                presets: ["env"],
                plugins: ["transform-runtime"]
              }
            }
          },
          {
            test: /\.html$/,
            use: {
              loader: "html-loader",
              options: htmlOptions
            }
          },
          {
            test: /\.css$/,
            use: extractStyles.extract({
              use: [
                {
                  loader: "css-loader",
                  options: cssOptions
                }
              ],
              fallback: "style-loader"
            })
          },
          {
            test: /\.less$/,
            use: extractStyles.extract({
              use: [
                {
                  loader: "css-loader",
                  options: cssOptions
                },
                {
                  loader: "less-loader",
                  options: lessOptions
                }
              ],
              fallback: "style-loader"
            })
          }
        ]
      },
      plugins: plugins
    },
    server.config.client.webpack.options
  );

  logger.info(
    `setup webpack with file watching ${options.watch ? "enabled" : "disabled"}`
  );

  return new Promise((resolve, reject) => {
    webpack(options, (err, stats) => {
      logStats(stats);
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};
