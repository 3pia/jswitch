/**
 * Helpful utilities.
 * @module util
 */

// node modules
const path = require("path");
const fs = require("fs");
const tty = require("tty");

// npm modules
const mkdirp = require("mkdirp");
const YAML = require("yamljs");

/**
 * Expands "~" and "$HOME" at the beginning of a path.
 * @param {string} value - The path to expand.
 * @return {string} - The expanded path.
 */
const userPath = function(value) {
  const home = process.env.HOME;

  for (const s of ["~", "$HOME"]) {
    if (value.substr(0, s.length) === s) {
      value = home + value.substr(s.length);
    }
  }

  return value;
};

/**
 * Reads a config file and returns the YAML parsed config (default) or the raw content.
 * @param {string} file - The config file to read.
 * @param {bool} parse=true - Whether or not to parse the content.
 * @return {string|object} - The parsed config or raw content.
 */
const readConfigSync = function(file, parse = true) {
  const content = fs.readFileSync(file, { encoding: "utf8" });
  return parse
    ? (file.endsWith(".json") ? JSON : YAML).parse(content)
    : content;
};

/**
 * Synchronously copy a file from a source to a destination file or directory and create all
 * intermediate directories.
 * @param {string} src - The source file to copy.
 * @param {string} dst - The destination, can be a file or directory.
 * @param {bool} overwrite=false - Whether or not to overwrite the destination if it exists.
 * @return {string} - The path of the copied destination.
 */
const copyFileSync = function(src, dst, overwrite = false) {
  // already existing?
  if (fs.existsSync(dst)) {
    const stats = fs.statSync(dst);
    if (stats.isDirectory()) {
      // dst is a directory, update it
      dst = path.join(dst, path.basename(src));
    }
    // check overwrite
    if (fs.existsSync(dst) && !overwrite) {
      return null;
    }
  }

  // recursively create dst directories
  mkdirp.sync(path.dirname(dst));

  // copy the file
  fs.copyFileSync(src, dst);
  return dst;
};

/**
 * Creates a symlink and ensures that intermediate directories exist.
 * @param {string} src - The link source.
 * @param {string} dst - The link destination.
 * @param {bool} overwrite=false - Whether or not to overwrite the destination of it exists.
 * @return {string} - The path of the linked destination.
 */
const createSymlinkSync = function(src, dst, overwrite = false) {
  // already existing?
  if (fs.existsSync(dst)) {
    const stats = fs.statSync(dst);
    if (stats.isDirectory()) {
      // dst is a directory, update it
      dst = path.join(dst, path.basename(src));
    }
    // check overwrite
    if (fs.existsSync(dst) && !overwrite) {
      return;
    }
  }

  // recursively create destination directories
  mkdirp.sync(path.dirname(dst));

  // create the link
  fs.symlinkSync(src, dst);
  return dst;
};

/**
 * Updates a koa context to send a json|plain response with status, message, and data fields.
 * @param {Context} ctx - The koa context object.
 * @param {object} data=null - Arbitrary response data.
 * @param {number} status=200 - An http status code.
 * @param {string} msg=null - A descriptive error message or null on success. When the status code
 * is a 2xx code, msg is set to null.
 * @param {bool} json - Whether or not to send json. When null, it is infered from the
 * "content-type" header.
 */
const respond = function(
  ctx,
  data = null,
  status = 200,
  msg = null,
  json = null
) {
  // detect json when not set
  if (json === null) {
    json = (ctx.header["content-type"] || "").match(/application\/json/gi);
  }

  // force msg to be null on success
  if (200 <= status && status < 300) {
    msg = null;
  }

  // status
  ctx.status = status;

  // body
  if (json) {
    ctx.body = { status, msg, data };
  } else {
    ctx.body = msg || "";
    if (data) {
      ctx.body += `\n\n${JSON.stringify(data)}`;
    }
  }
};

/**
 * Promisifies a child process and returns a new promise.
 * @param {Process} cp - A child process.
 * @return {Promise} - The created promise.
 * @example
 * const { exec } = require("child_process");
 * await promisifyChildProcess(exec("ls"));
 */
const promisifyChildProcess = function(cp) {
  return new Promise((resolve, reject) => {
    cp.addListener("error", reject);
    cp.addListener("exit", resolve);
  });
};

/**
 * Checks if the process' output stream is connected to a tty.
 * @return {bool} - Whether or not a tty is connected.
 */
const isTTY = function() {
  return tty.isatty(process.stdout.fd);
};

/**
 * Custom error with status and data fields.
 * @class
 */
class HTTPError extends Error {
  /**
   * @param {string} message - The error message.
   * @param {arbitrary} data=null - Arbitrary error data.
   * @param {number} status=400 - An error status code, most likely an http status.
   */
  constructor(message, data = null, status = 400) {
    super(message);

    this.data = data;
    this.status = status;
  }
}

/**
 * Custom error that is thrown when a user lacks a certain permission.
 * @class
 */
class PermissionError extends HTTPError {
  /**
   * @param {User} user - The user object.
   * @param {string} subject - Subject of the permission.
   */
  constructor(user, subject) {
    super(`User ${user.name} has no permission to ${subject}`, null, 403);
  }
}

/**
 * Exports.
 */

module.exports = {
  userPath,
  readConfigSync,
  copyFileSync,
  createSymlinkSync,
  respond,
  promisifyChildProcess,
  isTTY,
  HTTPError,
  PermissionError
};
