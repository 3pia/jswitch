/**
 * Database definition.
 */

// node modules
const fs = require("fs");

// npm modules
const Sequelize = require("sequelize");

/**
 * Database inferface.
 */
class DB {
  /**
   * @param {Server} server - The server instance.
   */
  constructor(server) {
    /** @member {Server} - The reference to the server instance. */
    this.server = server;

    /** @member {Object} - A sublogger spawned from the server logger object. */
    this.logger = server.logger.sub("db");

    /** @member {Object} - The Database config. */
    this.config = server.config.db;

    /** @member {Sequelize} - The Sequelize instance. */
    this.sql = null;
  }

  /**
   * Sets up the Sequelize instance, models and migrations. Called by the server.
   */
  async setup() {
    // get config
    const { name, user, pass, options = {}, migrations } = this.config;

    await this.server.hooks.call("db.setup.before");

    // define the sequelize instance
    options.operatorsAliases = false;
    this.sql = new Sequelize(name, user, pass, options);

    // authenticate
    await this.sql.authenticate();
    this.logger.info(`connected via ${this.sql.options.dialect}`);

    // load models
    await this.server.hooks.call("db.models.setup.before");
    const models = ["user", "permission"];
    for (const model of models) {
      // pass "sql, null, server" to all models to be consistent with the migration tool
      await this.server.hooks.call(`db.model.${model}.setup.before`);
      require("./models/" + model)(this.sql, null, this.server);
      await this.sql.sync();
      await this.server.hooks.call(`db.model.${model}.setup.after`);
    }
    this.logger.info(`loaded ${Object.keys(this.sql.models).length} model(s)`);
    await this.server.hooks.call("db.models.setup.after");

    // when migrations are enabled and there is no migrations meta table yet,
    // create and fill it according to sequelize v4
    if (migrations.enabled) {
      this.createMigrationsTable(migrations.tableName, "migrations");
    }

    await this.server.hooks.call("db.setup.after");
  }

  /**
   * Shuts down the Sequelize instance. Called by the server.
   */
  async shutdown() {
    if (this.sql) {
      await this.server.hooks.call("db.shutdown.before");
      await this.sql.close();
      await this.server.hooks.call("db.shutdown.after");

      this.logger.info("shutdown connection");
    }
  }

  /**
   * Reference to the loaded models, mapped to their names.
   * @type {Object}
   */
  get models() {
    return this.sql === null ? null : this.sql.models;
  }

  /**
   * Creates the migration table based on a directory containing migration files.
   * @param {string} tableName - The name of the table to create.
   * @param {string} migrationsDir - The directory that contains the migration files.
   */
  async createMigrationsTable(tableName, migrationsDir) {
    const q = this.sql.getQueryInterface();
    const tables = await q.showAllTables();
    if (!tables.includes(tableName)) {
      // create it
      await q.createTable(tableName, {
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
          primaryKey: true,
          autoIncrement: false
        }
      });
      this.logger.debug(`created migrations table "${tableName}"`);

      // fill it
      const names = fs
        .readdirSync(migrationsDir)
        .filter(f => f.match(/^\d+.*\.js$/));
      const items = names.sort().map(f => ({ name: f }));
      if (names.length > 0) {
        await q.bulkInsert(tableName, items);
        this.logger.debug(
          `added ${names.length} rows to migrations table "${tableName}"`
        );
      }
    }
  }
}

/**
 * Exports.
 */

module.exports = DB;
