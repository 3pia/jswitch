/**
 * Koa application routes.
 */

// local modules
const { respond, HTTPError } = require("./util");

module.exports = function(server) {
  const {
    router,
    db: { models },
    middleware: { user }
  } = server;

  const templateVars = {
    base: server.config.server.base,
    bundle: `${server.config.server.base}/static/bundle`
  };

  /**
   * Testing routes.
   */

  router.get("/api/ping", ctx => {
    ctx.body = "pong";
  });

  /**
   * Page routes.
   */

  router.get(["/", "/index"], user.redirect("/login"), async ctx => {
    await ctx.render("pages/index.pug", templateVars);
  });

  router.get("/login", user.redirect("/", true), async ctx => {
    await ctx.render("pages/login.pug", templateVars);
  });

  /**
   * Ajax routes.
   */

  router.put("/api/user", async ctx => {
    ctx
      .checkBody("username")
      .notEmpty()
      .len(4, 30);
    ctx
      .checkBody("email")
      .notEmpty()
      .isEmail();
    ctx
      .checkBody("password")
      .notEmpty()
      .len(8);
    if (ctx.errors) {
      throw new HTTPError("Invalid request body.", ctx.errors);
    }

    const { username, email, password } = ctx.request.body;
    const user = await models.User.register(username, email, password);

    ctx.session.userId = user.id;
    respond(ctx, { userId: user.id });
  });

  router.delete("/api/user", user, async ctx => {
    const user = await models.User.getById(ctx.session.userId);

    // remove the user and invalidate the session
    await user.remove();
    ctx.session = null;

    respond(ctx);
  });

  router.post("/api/session", async ctx => {
    ctx.checkBody("username").notEmpty();
    ctx.checkBody("password").notEmpty();
    if (ctx.errors) {
      throw new HTTPError("Invalid request body.", ctx.errors);
    }

    const { username, password } = ctx.request.body;
    const user = await models.User.login(username, password);

    ctx.session.userId = user.id;
    respond(ctx, { userId: user.id });
  });

  router.delete("/api/session", user, ctx => {
    ctx.session = null;
    respond(ctx);
  });
};
