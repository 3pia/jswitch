/**
 * Error catching and responding middleware.
 */

// local modules
const { respond, HTTPError } = require("../util");

module.exports = function(server) {
  return async function(ctx, next) {
    try {
      await next();
    } catch (err) {
      const { message, data = null, status = 400 } = err;
      respond(ctx, data, status, message);

      if (!(err instanceof HTTPError)) {
        server.logger.error(err);
      }
    }
  };
};
