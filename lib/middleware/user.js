/**
 * User authentication middleware.
 */

// local modules
const { respond } = require("../util");

module.exports = function(server) {
  const {
    db: { models }
  } = server;
  const { base } = server.config.server;

  // basic authentication method
  const authenticate = async function(ctx) {
    const user =
      Number.isInteger(ctx.session.userId) &&
      (await models.User.getById(ctx.session.userId));
    if (!user) {
      return false;
    } else {
      ctx.user = user;
      return true;
    }
  };

  // authentication middleware that responds with a status 403
  const auth = async function(ctx, next) {
    if (await authenticate(ctx)) {
      await server.hooks.call("middleware.user.auth.after", ctx.user);
      await next();
    } else {
      respond(ctx, null, 403, "Forbidden.");
    }
  };

  // authentication middleware that redicrects to a destination with a status 307
  auth.redirect = function(destination, reverse = false) {
    return async function(ctx, next) {
      if ((await authenticate(ctx)) != reverse) {
        await next();
      } else {
        ctx.status = 307;
        ctx.redirect(base + destination);
      }
    };
  };

  return auth;
};
