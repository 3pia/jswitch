/**
 * Static file serving middleware.
 */

// npm modules
const kSend = require("koa-send");

module.exports = function(root, base, opts = {}) {
  opts = Object.assign({ root }, opts);

  return async (ctx, next) => {
    let done = false;

    if (
      (ctx.method === "HEAD" || ctx.method === "GET") &&
      ctx.path.startsWith(base)
    ) {
      const file = ctx.path.substr(base.length);
      try {
        done = await kSend(ctx, file, opts);
      } catch (err) {
        if (err.status !== 404) {
          throw err;
        }
      }
    }

    if (!done) {
      await next();
    }
  };
};
