/**
 * Basic-auth middleware.
 */

// npm modules
const auth = require("basic-auth");

// local modules
const { respond } = require("../util");

module.exports = function({ name, pass }) {
  return async function(ctx, next) {
    const user = auth(ctx);

    if (!user || user.name !== name || user.pass != pass) {
      ctx.set("WWW-Authenticate", "Basic realm=jswitch");
      respond(ctx, null, 401, "Authentication required");
    } else {
      await next();
    }
  };
};
