/**
 * Command line interface based on argparse.
 * @module
 * @ignore
 */

// npm modules
const { ArgumentParser } = require("argparse");

// local modules
const util = require("./util");

/**
 * ArgumentParser setup.
 */

const parser = new ArgumentParser({
  description: "Starts the jswitch server.",
  version: util.readConfigSync("package.json").version,
  prog: "jswitch"
});

parser.addArgument(["--host", "-H"], {
  help: "server host"
});

parser.addArgument(["--port", "-P"], {
  help: "server port",
  type: "int"
});

parser.addArgument(["--base", "-b"], {
  help: "server base path",
  type: util.userPath
});

parser.addArgument(["--config-file", "-c"], {
  help: "path to the config file",
  dest: "configFile",
  type: util.userPath
});

parser.addArgument(["--log-level", "-l"], {
  help: "the console log level",
  dest: "logLevel"
});

parser.addArgument(["--log-file", "-f"], {
  help: "path to a log file, an absolute path is recommended",
  dest: "logFile",
  type: util.userPath
});

parser.addArgument(["--rebundle", "-r"], {
  help: "rebundle the client-side code",
  choices: ["yes", "no", "watch"]
});

/**
 * Exports.
 */

module.exports = function() {
  // parse args
  const args = parser.parseArgs();

  // merge some args with environment variables
  args.configFile = args.configFile || process.env.JSWITCH_CONFIG || null;

  return args;
};
