/**
 * Base interface for identity provider adapters.
 */

// npm modules
const { EventEmitter2 } = require("eventemitter2");
const Sequelize = require("sequelize");

/**
 * Base class definition.
 */

class ProviderAdapter extends EventEmitter2 {
  constructor(server, name, config) {
    super({ maxListeners: 50 });

    // components
    this.server = server;
    this.name = name;
    this.config = config;
    this.logger = null;
    this.models = null;
  }

  async setup() {
    this.setupLogging();
    await this.setupModels();

    this.logger.info("setup");
  }

  setupLogging() {
    this.logger = this.server.logger.sub(`idp.${this.name}:`);
  }

  async setupModels() {
    this.models = await this.defineModels(this.server.db.sql, Sequelize);

    if (Object.keys(this.models).length > 0) {
      await this.server.db.sql.sync();
    }
  }

  async defineModels() {
    // should be overwritten
    return {};
  }

  async sync(user, flush = true) {
    // fetch all permission names for this user
    const names = await this.fetch(user);

    // add them to the db
    if (flush) {
      await this.server.db.models.Permission.syncByUserAndProvider(
        user,
        this.name,
        names
      );
    } else {
      await this.server.db.models.Permission.bulkAdd(user, this.name, names);
    }
  }

  async fetch(user) {
    // should be overwritten
    return [];
  }
}

/**
 * Exports.
 */

module.exports = ProviderAdapter;
