/**
 * User model.
 */

// npm modules
const Sequelize = require("sequelize");
const argon2 = require("argon2");

// local modules
const { HTTPError } = require("../util");

module.exports = function(sql, _, server) {
  /**
   * Model definition.
   */

  const User = sql.define("User", {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true,
      autoIncrement: true
    },
    name: {
      type: Sequelize.STRING(50),
      unique: "nameIndex",
      allowNull: false,
      validate: { len: [1, 50] }
    },
    email: {
      type: Sequelize.STRING(50),
      unique: "emailIndex",
      allowNull: false,
      validate: { len: [5, 50] }
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    status: {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    hash: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    }
  });

  /**
   * Class attributes.
   */

  User.INACTIVE = 0;
  User.ACTIVE = 1;
  User.DELETED = 2;

  /**
   * Class methods.
   */

  User.exists = async function(id) {
    return User.getById(id, ["id"]) !== null;
  };

  User.getById = async function(id, attributes = null) {
    return await User.findOne({ where: { id }, attributes });
  };

  User.getByName = async function(name, attributes = null) {
    return await User.findOne({ where: { name }, attributes });
  };

  User.getByEmail = async function(email, attributes = null) {
    return await User.findOne({ where: { email }, attributes });
  };

  User.register = async function(name, email, password) {
    if (
      (await User.getByName(name, ["id"])) ||
      (await User.getByEmail(email, ["id"]))
    ) {
      throw new HTTPError("User already exists.");
    }

    const hash = await argon2.hash(password);
    const user = await User.build({ name, email, password: hash });

    await server.hooks.call("user.register.before", user);

    await user.save();

    await server.hooks.call("user.register.after", user);

    return user;
  };

  User.login = async function(nameOrEmail, password) {
    const user =
      (await User.getByName(nameOrEmail)) ||
      (await User.getByEmail(nameOrEmail));
    if (!user) {
      throw new HTTPError("Username and password do not match.");
    }

    const match = await argon2.verify(user.password, password);
    if (!match) {
      throw new HTTPError("Username and password do not match.");
    }

    await server.hooks.call("user.login.after", user);

    return user;
  };

  /**
   * Instance methods.
   */

  User.prototype.remove = async function() {
    // remove all permissions
    const permissions = await server.db.models.Permission.getByUser(this);
    for (const permission of permissions) {
      await permission.remove(this);
    }

    // finally, remove the user instance
    await this.destroy();
  };

  return User;
};
