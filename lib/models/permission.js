/**
 * Permission model.
 */

// npm modules
const Sequelize = require("sequelize");

module.exports = function(sql, _, server) {
  /**
   * Model definition.
   */

  const Permission = sql.define("Permission", {
    userId: {
      type: Sequelize.INTEGER,
      allowNull: false,
      primaryKey: true,
      unique: "UserIdProviderNameIndex",
      references: {
        model: "User",
        key: "id"
      }
    },
    provider: {
      type: Sequelize.STRING(50),
      allowNull: false,
      primaryKey: true,
      unique: "UserIdProviderNameIndex"
    },
    name: {
      type: Sequelize.STRING(100),
      allowNull: false,
      primaryKey: true,
      unique: "UserIdProviderNameIndex"
    }
  });

  /**
   * Helpers.
   */

  // only search with $like if the name actually contains a wildcard for performance
  const wildcardAware = name => (name.includes("%") ? { $like: name } : name);

  /**
   * Class methods.
   */

  Permission.get = async function(user, provider, name, attributes = null) {
    return await Permission.findOne({
      where: { userId: user.id, provider, name: wildcardAware(name) },
      attributes
    });
  };

  Permission.getByUser = async function(user, attributes = null) {
    return await Permission.findAll({ where: { userId: user.id }, attributes });
  };

  Permission.getByUserAndProvider = async function(
    user,
    provider,
    attributes = null
  ) {
    return await Permission.findAll({
      where: { userId: user.id, provider },
      attributes
    });
  };

  Permission.getByProviderAndName = async function(
    provider,
    name,
    attributes = null
  ) {
    return await Permission.findAll({
      where: { provider, name: wildcardAware(name) },
      attributes
    });
  };

  Permission.exists = async function(user, provider, name) {
    const where = { userId: user.id, provider, name: wildcardAware(name) };
    return (await Permission.count({ where })) === 1;
  };

  Permission.add = async function(user, provider, name) {
    // already existing?
    let permission = await Permission.get(user, provider, name);
    if (permission) {
      return permission;
    }

    permission = await Permission.build({ userId: user.id, provider, name });

    await server.hooks.call("db.model.permission.add.before", permission);

    await permission.save();

    await server.hooks.call("db.model.permission.add.after", permission);

    return permission;
  };

  Permission.bulkAdd = async function(user, provider, names) {
    const permissions = names.map(name => ({
      userId: user.id,
      provider,
      name
    }));
    await Permission.bulkCreate(permissions);
  };

  Permission.removeByUser = async function(user) {
    // TODO: is the user allowed to do that?
    await Permission.destroy({ where: { userId: user.id } });
  };

  Permission.removeByUserAndProvider = async function(user, provider) {
    // TODO: is the user allowed to do that?
    await Permission.destroy({ where: { userId: user.id, provider } });
  };

  Permission.syncByUser = async function(user, permissions) {
    // first, delete all permissions of that user
    await Permission.removeByUser(user);

    // permissions must be a list of { provider, name } objects
    // add the user id to each of them
    permissions = permissions.map(p =>
      Object.assign({}, p, { userId: user.id })
    );

    // create all of them
    await Permission.bulkCreate(permissions);
  };

  Permission.syncByUserAndProvider = async function(user, provider, names) {
    // first, delete all permissions of that user for that provider
    await Permission.removeByUserAndProvider(user, provider);

    // create the list of permissions
    const permissions = names.map(name => ({ user, provider, name }));

    // create all of them
    await Permission.bulkCreate(permissions);
  };

  /**
   * Instance methods.
   */

  Permission.prototype.remove = async function(user) {
    // TODO: is the user allowed to do that?
    await this.destroy();
  };

  return Permission;
};
