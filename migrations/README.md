# Database Migrations

### Install `sequelize-cli`

```bash
npm install -g sequelize-cli
```

### Run migrations

```bash
export JSWITCH_CONFIG=conf/config.yml # or some other file

sequelize db:migrate
```
