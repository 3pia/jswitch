/**
 * User-defined events.
 * The jswitch server publishes events and does not wait for their completion. Multiple listener
 * callback can be registered for one event. In addition, each *hook* also emits an event with
 * identical name and arguments. See conf/samples/hooks.js for a list of currently available hooks.
 * However, the event mechanism should not be confused with hooks which rather follow a call-listen
 * paradigm.
 */

module.exports = async function(server) {
  // method 1: programmatically add events to the server
  // example:
  // server.events.on("server.setup.before", () => {
  //     ...
  // });
  // method 2: return a mapping of event name to listeners
  // example:
  // return {"server.setup.before": [() => {
  //     ...
  // }]};
};
