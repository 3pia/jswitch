# jswitch

[![build status](https://git.rwth-aachen.de/3pia/jswitch/badges/master/build.svg)](https://git.rwth-aachen.de/3pia/jswitch/commits/master) [![docs status](https://img.shields.io/badge/docs-build-brightgreen.svg)](http://3pia.pages.rwth-aachen.de/jswitch) [![coverage report](https://git.rwth-aachen.de/3pia/jswitch/badges/master/coverage.svg)](https://git.rwth-aachen.de/3pia/jswitch/commits/master)

## Development

### Development process

We should follow a git workflow with `feature` and `release` branches, and release and release candidate tags.

![cycle](https://rwth-aachen.sciebo.de/index.php/apps/files_sharing/ajax/publicpreview.php?x=1798&y=754&a=true&file=cycle_small.png&t=JUyt6ZO6Cab0wYh&scalingup=0)

###### Features and bugfixes

- The `master` branch is protected and should be kept clean and up-to-date.
- For features, fixes, etc, create feature branches based off the `master` branch.
- Create [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) to merge your (tested) feautures back into the `master` branch.

###### Releases

- For preparing a release, we create a `release/v<major>.<minor>` branch based off the `master` branch.
- Tags of commits on release branches are named `v<major>.<minor>.<patch>-<stage>`, where the first `patch` number is 0 and the `stage` is typically `rc0`, `rc1`, etc.
- Use branches prefixed with `hotfix/` to fix bugs in `release` branches.
- When a release is ready to be published, we create a new tag named `v<major>.<minor>.<patch>`.
- The changes due to hotfixes should be added to the `master` branch again by either
  - merging **without** fast-forwarding, or
  - cherrpy-picking certain commits from `hotfix` branches.

###### Release and tag versioning

- Release branches: `release/v<major>.<minor>`
- Tag on release branches: `v<major>.<minor>.<patch>-<stage>`
- All other tags must not start with `v\d`.

###### Branch naming

Except for the `master` branch, we should use the `<purpose>/<description>` convention.

`purpose` should be one of the following:

- `<username>` for personal branches.
- `feature` for feature branches.
- `fix` for bug fixes on the master branch.
- `release` for final releases (with `description` as defined above).
- `hotfix` for bug fixes on release branches.

### Docker

You can find all images at our [Docker Hub](https://hub.docker.com/r/3pia).

We provide a dedicated image containing everything to run the jswitch server. This image can also be used for development:

```bash
git checkout git@git.rwth-aachen.de:3pia/jswitch.git
cd jswitch

# run the server inside docker
docker run --name jsdev -ti -p 4282:4282 -v `pwd`:/jswitch/repo 3pia/jswitch
```

- `--name jsdev` names the container "jsdev" so you can start and attach it later on without looking up the hash.
- `-ti` allocates a pseudo-tty and keeps `stdin` open.
- `-p 4282:4282` publishes the application port (second number) to the host system (first number).
- `` -v `pwd`:/jswitch/repo `` mounts the current host directory to the container directory that is supposed to contain the jswitch repository.

The container is not deleted when you stop it (no `--rm` parameter). If you want to reattach it to start developing again, do:

```bash
docker start -i jsdev
```
